import 'package:flutter_design/com/qlslylq/flutterdesign/bean/AdmxUser.dart';
import 'package:json_annotation/json_annotation.dart';

/*
 * 用户信息<br/>
 */
part 'User.g.dart';
@JsonSerializable()
class User extends AdmxUser {

  @JsonKey(name: '主键')
  String id;

  String uid;

  @JsonKey(name: '账号')
  String username;

  List<String> avatars;

  String avatar;

  @JsonKey(name: '角色')
  String role;

  @JsonKey(name: '性别')
  String sex;

  @JsonKey(name: '出生日期')
  String birthday;

  @JsonKey(name: '手机号')
  String mobile;

  @JsonKey(name: '创建时间')
  String createtime;

  @JsonKey(name: '更新时间')
  String updatetime;

  @JsonKey(name: '创建人')
  String creater;

  String getUid() {
    return uid;
  }

  User() {

  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}

