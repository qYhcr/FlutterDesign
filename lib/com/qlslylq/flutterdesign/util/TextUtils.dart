/*
 * 文本工具类<br/>
 */
class TextUtils {

  /*
   * 是否为空字符串<br/>
   */
  static bool isEmpty(String text) {
    if (text == null || text.length == 0) {
      return true;
    }
    return false;
  }
}