/*
 * 字符串去空工具类<br/>
 */
class StringUtils {

  /*
   * 去除以下字符<br>
   * \n 回车(\u000a)<br>
   * \t 水平制表符(\u0009)<br>
   * \s 空格(\u0008)<br>
   * \r 换行(\u000d)<br>
   */
  static String replaceBlank(String str) {
    String dest = "";
    if (str != null) {
//      Pattern p = Pattern.compile("\\s*|\t|\r|\n");
//      Matcher m = p.matcher(str);
//      dest = m.replaceAll("");
    }
    return dest;
  }

  /*
   * 去除以下字符,及头尾去空<br>
   * \t 水平制表符(\u0009)<br>
   */
  static String trim(String str) {
    String dest = "";
    if (str != null) {
//      dest = str.replaceAll("(^[ |　]*|[ |　]*$)", "").trim();
    }
    return dest;
  }

  /*
   * 去除以下字符<br>
   * ["<br/>
   * "]<br/>
   */
  static String replaceArray(String str) {
    String dest = "";
    if (str != null) {
//      dest = str.replace("[\"", "").replace("\"]", "");
    }
    return dest;
  }

  /*
   * 转换网页换行符<br>
   */
  static String replaceEnter(String str) {
    String dest = "";
    if (str != null) {
      dest = str.replaceAll("<br/>", "\n");
    }
    return dest;
  }
}
