/*
 * 数据响应事件 <br/>
 */
abstract class HttpListener {

  void onDbSucceed(String method, Object values);

  void onDbFaild(String method, Object values);

  void onNetWorkSucceed(String method, Object values);

  void onNetWorkFaild(String method, Object values);

  void onOtherSucceed(String method, Object values);

  void onOtherFaild(String method, Object values);

  void onException(String method, Object e);

  void onCancel(String method);

}
