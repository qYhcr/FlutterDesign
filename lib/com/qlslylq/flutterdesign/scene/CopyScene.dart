import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/scene/BaseScene.dart';

void main() {
  runApp(new CopyScene());
}

/*
 * 模板页 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class CopyScene extends BaseScene {

  State<StatefulWidget> createState() {
    return new CopySceneState();
  }

}

/*
 * 页面功能 <br/>
 */
class CopySceneState extends BaseSceneState {

  CopySceneState() {
    title = 'FlutterDesign';
    setRightButtonFromIcon(Icons.menu);
    setBorderButtonFromIcon(Icons.add);
  }

  Widget build(BuildContext context) {
    return buildBody(context,
        new Text('测试内容')
    );
  }

  void onClick(Widget widget) {
    if (widget.key == key_btn_right) {

    } else if (widget.key == key_btn_border) {

    }
  }
}